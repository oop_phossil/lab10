package com.phossil.week10;

public class Triangle extends Shape{
    private int side1;
    private int side2;
    private int side3;

    public Triangle(int side1, int side2, int side3) {
        super("Triangle");
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
    }

    
    @Override
    public String toString() {
        return this.getName() + " A:" + this.side1 + " B:" + this.side2 + " C:" + this.side3;
    }


    @Override
    public double calArea() {
        float findS = (side1 + side2 + side3) / 2;
        float findAll = (findS-side1)*(findS-side2)*(findS-side3);
        float areas = (float) Math.sqrt(findS*findAll);
        return areas;
    }



    @Override
    public double calPerimeter() {
        int lines = side1+side2+side3;
        return lines;
    }

}

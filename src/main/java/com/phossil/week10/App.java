package com.phossil.week10;

public class App 
{
    public static void main( String[] args )
    {
        Rectangle rec = new Rectangle(5, 3);
        System.out.println( rec.toString() );
        System.out.println( rec.calArea() );
        System.out.println( rec.calPerimeter() );

        Rectangle rec2 = new Rectangle(2, 2);
        System.out.println( rec2.toString() );
        System.out.println( rec2.calArea() );
        System.out.println( rec2.calPerimeter() );

        Circle circle1 = new Circle(2);
        System.out.println(circle1.toString());
        System.out.printf("%s area: %.3f \n" , circle1.getName() , circle1.calArea());
        System.out.printf("%s perimeter: %.3f \n" , circle1.getName() , circle1.calPerimeter());

        Circle circle2 = new Circle(3);
        System.out.println(circle2.toString());
        System.out.printf("%s area: %.3f \n" , circle2.getName() , circle2.calArea());
        System.out.printf("%s perimeter: %.3f \n" , circle2.getName() ,circle2.calPerimeter());

        Triangle triangle1 = new Triangle(5, 5, 4);
        System.out.println(triangle1.toString());
        System.out.printf("%.3f\n",triangle1.calArea() );
        System.out.println(triangle1.calPerimeter() );

        Triangle triangle2 = new Triangle(8, 10, 12);
        System.out.println(triangle2.toString());
        System.out.printf("%.3f\n",triangle2.calArea() );
        System.out.println(triangle2.calPerimeter() );
    
    }
}
